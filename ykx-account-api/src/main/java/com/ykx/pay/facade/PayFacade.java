package com.ykx.pay.facade;

import org.mengyun.tcctransaction.api.Compensable;

import com.ykx.base.model.RspValue;
import com.ykx.pay.model.ReqPay;

public interface PayFacade {
	@Compensable 
	public RspValue savePay(ReqPay reqPay) throws Exception;
	
	
	
	
}
