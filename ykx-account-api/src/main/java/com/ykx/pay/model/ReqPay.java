package com.ykx.pay.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class ReqPay implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String orderId;
	private BigDecimal payMoney;
	private String productId;
	private BigDecimal price;
	private int amount;
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public BigDecimal getPayMoney() {
		return payMoney;
	}
	public void setPayMoney(BigDecimal payMoney) {
		this.payMoney = payMoney;
	}
	
	
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "ReqPay [orderId=" + orderId + ", payMoney=" + payMoney + ", productId=" + productId + ", price=" + price
				+ ", amount=" + amount + "]";
	}

	
	
	

}
