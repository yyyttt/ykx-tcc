package com.ykx.pay.dao;

import com.ykx.pay.model.Taccount;

public interface AccountDao {

	
	public Taccount getAccountById(Integer id) throws Exception;
	
	public void updateAccount(Taccount account) throws Exception;
}
