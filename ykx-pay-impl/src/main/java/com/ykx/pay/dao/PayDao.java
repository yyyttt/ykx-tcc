package com.ykx.pay.dao;

import com.ykx.pay.model.Tpay;

public interface PayDao {

	public void save(Tpay pay) throws Exception;
}
