package com.ykx.pay.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ykx.pay.dao.AccountDao;
import com.ykx.pay.model.Taccount;
@Repository("accountDao")
public class AccountDaoImpl implements AccountDao{
	@Autowired
	private SqlSessionTemplate  sqlSession;
	
	@Override
	public Taccount getAccountById(Integer id) throws Exception {
		Map<String,Integer> map = new HashMap<String,Integer>();
		map.put("id", id);
		return sqlSession.selectOne("com.ykx.pay.dao.AccountDao.getAccountById",map);
	}

	@Override
	public void updateAccount(Taccount account) throws Exception {
		// TODO Auto-generated method stub
		sqlSession.update("com.ykx.pay.dao.AccountDao.updateAccount", account);
	}

}
