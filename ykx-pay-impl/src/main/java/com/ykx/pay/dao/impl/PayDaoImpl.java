package com.ykx.pay.dao.impl;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ykx.pay.dao.PayDao;
import com.ykx.pay.model.Tpay;
@Repository("payDao")
public class PayDaoImpl implements PayDao{
	@Autowired
	private SqlSessionTemplate  sqlSession;
	@Override
	public void save(Tpay pay) throws Exception {
		// TODO Auto-generated method stub
		sqlSession.insert("com.ykx.pay.dao.PayDao.save",pay);
	}

}
