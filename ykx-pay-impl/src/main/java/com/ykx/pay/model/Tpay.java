package com.ykx.pay.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Tpay implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String orderId;
	private BigDecimal pay;
	private Date createTime;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public BigDecimal getPay() {
		return pay;
	}
	public void setPay(BigDecimal pay) {
		this.pay = pay;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	@Override
	public String toString() {
		return "Tpay [id=" + id + ", orderId=" + orderId + ", pay=" + pay + ", createTime=" + createTime + "]";
	}
	
	

}
