package com.ykx.pay.service.impl;

import org.mengyun.tcctransaction.api.Compensable;
import org.mengyun.tcctransaction.api.TransactionContext;
import org.mengyun.tcctransaction.context.MethodTransactionContextEditor;
import org.springframework.stereotype.Service;

import com.ykx.base.model.RspValue;
import com.ykx.pay.facade.PayFacade2;
import com.ykx.pay.model.ReqPay;
@Service("payFacade2")  
public class PayFacade2Impl implements PayFacade2{

	@Override
	@Compensable(confirmMethod = "confirmSavePay", 
	             cancelMethod = "cancelSavePay", 
	             transactionContextEditor = MethodTransactionContextEditor.class) 
	public RspValue savePay(TransactionContext transactionContext,ReqPay reqPay) throws Exception {
		System.out.println("=================2========================");  
	    System.out.println("保存支付信息,更新库存2....");  
	    System.out.println("================2=========================");
	    RspValue rv = new RspValue();
		return rv;
	}

	public RspValue confirmSavePay(TransactionContext transactionContext,ReqPay reqPay) throws Exception {
		System.out.println("@@@@@@@@@@@@@@@@@22@@@@@@@@@@@@@@@@@@@@@@@@");  
	    System.out.println("confirmSavePay222,保存支付信息....");  
	    System.out.println("@@@@@@@@@@@@@@@@@222@@@@@@@@@@@@@@@@@@@@@@@@");
	    RspValue rv = new RspValue();
		return rv;
	}

	public RspValue cancelSavePay(TransactionContext transactionContext,ReqPay reqPay) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("---------------------2---------------------");  
	    System.out.println("cancelSavePay2,回滚刚才的支付信息");  
	    System.out.println("------------------2-------------------------");
	    RspValue rv = new RspValue();
		return rv;
	}

	

}
