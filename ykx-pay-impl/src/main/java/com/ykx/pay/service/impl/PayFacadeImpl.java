package com.ykx.pay.service.impl;

import org.mengyun.tcctransaction.api.Compensable;
import org.mengyun.tcctransaction.dubbo.context.DubboTransactionContextEditor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ykx.base.model.RspValue;
import com.ykx.pay.facade.PayFacade;
import com.ykx.pay.model.ReqPay;
@Service("payFacade")  
public class PayFacadeImpl implements PayFacade{

	@Override
	@Compensable(confirmMethod = "confirmSavePay", 
	             cancelMethod = "cancelSavePay", 
	             transactionContextEditor = DubboTransactionContextEditor.class) 
	public RspValue savePay(ReqPay reqPay) throws Exception {
		System.out.println("=========================================");  
	    System.out.println("保存支付信息,更新库存....");  
	    System.out.println("=========================================");
	    RspValue rv = new RspValue();
		return rv;
	}

	public RspValue confirmSavePay(ReqPay reqPay) throws Exception {
		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");  
	    System.out.println("confirmSavePay,保存支付信息....");  
	    System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
	    RspValue rv = new RspValue();
		return rv;
	}

	public RspValue cancelSavePay(ReqPay reqPay) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("------------------------------------------");  
	    System.out.println("cancelSavePay,回滚刚才的支付信息");  
	    System.out.println("-------------------------------------------");
	    RspValue rv = new RspValue();
		return rv;
	}

	

}
