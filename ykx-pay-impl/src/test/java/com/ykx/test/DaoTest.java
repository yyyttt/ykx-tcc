package com.ykx.test;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ykx.pay.dao.AccountDao;
import com.ykx.pay.dao.PayDao;
import com.ykx.pay.model.Taccount;
import com.ykx.pay.model.Tpay;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {  "classpath:beans/spring-context.xml" })
public class DaoTest {

	@Autowired
	private PayDao payDao;
	@Autowired
	private AccountDao accountDao;
	@Test
	public void testPay() throws Exception{
		Tpay pay=new Tpay();
		pay.setId(1);
		pay.setCreateTime(new Date());
		pay.setPay(new BigDecimal("100"));
		pay.setOrderId("orderId");
		payDao.save(pay);
	}
	
	@Test
	public void testAccount() throws Exception{
		Taccount account = accountDao.getAccountById(1);
		System.out.println(account);
		account.setBalance(new BigDecimal(6666));
		accountDao.updateAccount(account);
	}
	
	
}
