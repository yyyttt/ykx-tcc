package com.ykx.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {  "classpath:beans/spring-context.xml" })
public class DubboTest {
	@Test
	public void pay() throws Exception{
		System.out.println("----------");
		Thread.sleep(1000000);
	}
}
