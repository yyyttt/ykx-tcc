package com.ykx.product.facade;

import org.mengyun.tcctransaction.api.Compensable;

import com.ykx.base.model.RspValue;
import com.ykx.pay.model.ReqPay;
import com.ykx.product.model.ReqProduct;

public interface ProductFacade {

	public RspValue payProduct(ReqProduct reqProduct) throws Exception;
	
	@Compensable 
	public RspValue updateProduct(ReqPay reqPay) throws Exception;
	
	
}
