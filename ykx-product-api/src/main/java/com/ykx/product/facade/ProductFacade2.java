package com.ykx.product.facade;

import org.mengyun.tcctransaction.api.TransactionContext;

import com.ykx.base.model.RspValue;
import com.ykx.pay.model.ReqPay;
import com.ykx.product.model.ReqProduct;

public interface ProductFacade2 {

	public RspValue payProduct(ReqProduct reqProduct) throws Exception;
	
	public RspValue updateProduct(TransactionContext transactionContext,ReqPay reqPay) throws Exception;
	
	
}
