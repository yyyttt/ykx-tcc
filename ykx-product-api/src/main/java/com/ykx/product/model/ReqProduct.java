package com.ykx.product.model;

import java.io.Serializable;

public class ReqProduct implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String orderId;
	private String productId;
	private int amount;
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "ReqProduct [orderId=" + orderId + ", productId=" + productId + ", amount=" + amount + "]";
	}
	
	
	
	

}
