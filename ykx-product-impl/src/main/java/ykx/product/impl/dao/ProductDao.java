package ykx.product.impl.dao;

import ykx.product.impl.model.Tproduct;

public interface ProductDao {

	public Tproduct getProductById(int id) throws Exception;
	
	public void updateProduct(Tproduct product) throws Exception;
}
