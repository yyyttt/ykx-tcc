package ykx.product.impl.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ykx.product.impl.dao.ProductDao;
import ykx.product.impl.model.Tproduct;
@Repository("productDao")
public class ProductDaoImpl implements ProductDao{

	@Autowired
	private SqlSessionTemplate  sqlSession;
	
	@Override
	public Tproduct getProductById(int id) throws Exception {
		Map<String,Integer> map = new HashMap<String,Integer>();
		map.put("id", id);
		return sqlSession.selectOne("ykx.product.impl.dao.ProductDao.getProductById",map);
	}

	@Override
	public void updateProduct(Tproduct product) throws Exception {
		// TODO Auto-generated method stub
		sqlSession.update("ykx.product.impl.dao.ProductDao.updateProduct",product);
	}

}
