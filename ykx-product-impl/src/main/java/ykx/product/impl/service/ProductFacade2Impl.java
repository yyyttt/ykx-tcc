package ykx.product.impl.service;

import org.mengyun.tcctransaction.api.Compensable;
import org.mengyun.tcctransaction.api.TransactionContext;
import org.mengyun.tcctransaction.context.MethodTransactionContextEditor;
import org.springframework.stereotype.Repository;

import com.ykx.base.model.RspValue;
import com.ykx.pay.model.ReqPay;
import com.ykx.product.facade.ProductFacade2;
import com.ykx.product.model.ReqProduct;
@Repository("productFacade2")
public class ProductFacade2Impl implements ProductFacade2{

	@Override
	public RspValue payProduct(ReqProduct reqProduct) throws Exception {
		RspValue rv = new RspValue();
		return rv;
	}
	
	@Override
	@Compensable(confirmMethod = "confirmupdateProduct", 
                 cancelMethod = "cancelupdateProduct", 
                 transactionContextEditor = MethodTransactionContextEditor.class)
//	@Transactional
	public RspValue updateProduct(TransactionContext transactionContext,ReqPay reqPay) throws Exception {
		System.out.println("========2=================================");  
	    System.out.println("更新库存2....");  
	    System.out.println("=========2================================");
	    
	    RspValue rv = new RspValue();
		return rv;
	}

	public RspValue confirmupdateProduct(TransactionContext transactionContext,ReqPay reqPay) throws Exception {
		System.out.println("@@@@@@@@@@@@@@@@@2@@@@@@@@@@@@@@@@@@@@@@@@");  
	    System.out.println("confirmupdateProduct2,更新库存....");  
	    System.out.println("@@@@@@@@@@@@@@@@@@2@@@@@@@@@@@@@@@@@@@@@@@");
	    RspValue rv = new RspValue();
		return rv;
	}

	public RspValue cancelupdateProduct(TransactionContext transactionContext,ReqPay reqPay) throws Exception {
		System.out.println("-------------------2-----------------------");  
	    System.out.println("cancelupdateProduct2,回滚库存");  
	    System.out.println("-------------------2------------------------");
	    RspValue rv = new RspValue();
		return rv;
	}

}
