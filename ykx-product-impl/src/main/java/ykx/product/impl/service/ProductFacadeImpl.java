package ykx.product.impl.service;

import org.mengyun.tcctransaction.api.Compensable;
import org.mengyun.tcctransaction.dubbo.context.DubboTransactionContextEditor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ykx.base.model.RspValue;
import com.ykx.pay.model.ReqPay;
import com.ykx.product.facade.ProductFacade;
import com.ykx.product.model.ReqProduct;
@Repository("productFacade")
public class ProductFacadeImpl implements ProductFacade{

	@Override
	public RspValue payProduct(ReqProduct reqProduct) throws Exception {
		RspValue rv = new RspValue();
		return rv;
	}
	
	@Override
	@Compensable(confirmMethod = "confirmupdateProduct", 
                 cancelMethod = "cancelupdateProduct", 
                 transactionContextEditor = DubboTransactionContextEditor.class)
//	@Transactional
	public RspValue updateProduct(ReqPay reqPay) throws Exception {
		System.out.println("=========================================");  
	    System.out.println("更新库存....");  
	    System.out.println("=========================================");
	    
	    RspValue rv = new RspValue();
		return rv;
	}

	public RspValue confirmupdateProduct(ReqPay reqPay) throws Exception {
		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");  
	    System.out.println("confirmupdateProduct,更新库存....");  
	    System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
	    RspValue rv = new RspValue();
		return rv;
	}

	public RspValue cancelupdateProduct(ReqPay reqPay) throws Exception {
		System.out.println("------------------------------------------");  
	    System.out.println("cancelupdateProduct,回滚库存");  
	    System.out.println("-------------------------------------------");
	    RspValue rv = new RspValue();
		return rv;
	}

}
