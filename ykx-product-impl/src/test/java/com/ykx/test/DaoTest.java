package com.ykx.test;

import java.io.Serializable;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ykx.product.impl.dao.ProductDao;
import ykx.product.impl.model.Tproduct;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {  "classpath:beans/spring-context.xml" })
public class DaoTest {

	@Autowired
	private ProductDao productDao;
	
	@Test
	public void testProduct() throws Exception{
		Tproduct product = productDao.getProductById(1);
		product.setAmount(6666);
		productDao.updateProduct(product);
	}
}
