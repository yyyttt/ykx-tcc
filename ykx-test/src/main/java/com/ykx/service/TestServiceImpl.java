package com.ykx.service;

import java.math.BigDecimal;

import org.mengyun.tcctransaction.api.Compensable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ykx.base.model.RspValue;
import com.ykx.pay.facade.PayFacade;
import com.ykx.pay.facade.PayFacade2;
import com.ykx.pay.model.ReqPay;
import com.ykx.product.facade.ProductFacade;
import com.ykx.product.facade.ProductFacade2;

@Service("testService")
public class TestServiceImpl implements TestService{
	
	@Autowired
	private PayFacade payFacade;
	@Autowired
	private ProductFacade productFacade;
	@Autowired
	private PayFacade2 payFacade2;
	@Autowired
	private ProductFacade2 productFacade2;
	
	@Transactional
//	@Compensable(confirmMethod = "confirmPayPay", cancelMethod = "cancelMakePayPay")
	@Compensable(confirmMethod = "confirmPayPay", cancelMethod = "cancelMakePayPay")
	public void payPay() throws Exception{
		ReqPay reqPay = new ReqPay();
		reqPay.setAmount(10);
		reqPay.setOrderId("123456");
		reqPay.setPayMoney(new BigDecimal(100));
		reqPay.setPrice(new BigDecimal(10));
		reqPay.setProductId("1");;
//		RspValue rv = payFacade.savePay(reqPay);
//		RspValue rv2= productFacade.updateProduct(reqPay);
		
		RspValue rv = payFacade2.savePay(null,reqPay);
		RspValue rv2= productFacade2.updateProduct(null,reqPay);
		System.out.println(rv);
		System.out.println(rv2);
		
	}
	public void confirmPayPay() throws Exception{
		System.out.println("--confirmPayPay------");
	}
	
    public void cancelMakePayPay() throws Exception{
    	System.out.println("--cancelMakePayPay------");
	}
	
}
