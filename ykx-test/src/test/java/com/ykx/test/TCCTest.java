package com.ykx.test;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ykx.base.model.RspValue;
import com.ykx.pay.facade.PayFacade;
import com.ykx.pay.model.ReqPay;
import com.ykx.product.facade.ProductFacade;
import com.ykx.product.model.ReqProduct;
import com.ykx.service.TestService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {  "classpath:beans/spring-context.xml" })
public class TCCTest {

	@Autowired
	private ProductFacade productFacade;
//	@Autowired
//	private PayFacade payFacade;
	@Autowired
	private TestService testService;
	
	@Test
	public void payPay() throws Exception{
//		ReqProduct reqProduct = new ReqProduct();
//		RspValue rv = productFacade.payProduct(reqProduct);
//		System.out.println(rv);
//		ReqPay reqPay = new ReqPay();
//		reqPay.setAmount(10);
//		reqPay.setOrderId("123456");
//		reqPay.setPayMoney(new BigDecimal(100));
//		reqPay.setPrice(new BigDecimal(10));
//		reqPay.setProductId("1");;
//		RspValue rv = payFacade.savePay(reqPay);
//		System.out.println(rv);
		
		testService.payPay();
		Thread.sleep(10000000);
	}
}
